function kiemTraTrong(value, hienThiErr) {
    if (value == "") {
        document.getElementById(hienThiErr).innerText = "Mục không được để trống";
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
    else {
        document.getElementById(hienThiErr).style.display = "none";
        return true;
    }
}

function kiemTraTrung(id, userArr, hienThiErr) {
    var viTri = userArr.findIndex(function(item) {
        return item.taiKhoan == id;
    });
    if (viTri != -1) {
        document.getElementById(hienThiErr).innerText = "Tài khoản đã tồn tại";
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
    else {
        document.getElementById(hienThiErr).style.display = "none";
        return true;
    }
}

function kiemTraChu(value, hienThiErr) {
    const re = /^[a-zA-Z\s]*$/;
    var isLetter = re.test(value);
    if (isLetter) {
        document.getElementById(hienThiErr).style.display = "none";
        return true;
    }
    else {
        document.getElementById(hienThiErr).innerText = `Tên user phải là chữ không dấu và không được chứa số, ký tự đặc biệt`;
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
}
function kiemTraMatKhau(value, hienThiErr) {
    const re = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{6,10}$/;
    var isMatKhau = re.test(value);
    if (isMatKhau) {
        document.getElementById(hienThiErr).style.display = "none";
        return true;
    }
    else {
        document.getElementById(hienThiErr).innerText = `Mật khẩu phải từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`;
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
}

function kiemTraEmail(value, hienThiErr) {
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(value);
    if (isEmail) {
        document.getElementById(hienThiErr).innerText = "";
        return true;
    }
    else {
        document.getElementById(hienThiErr).innerText = `Email không đúng định dạng`;
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
}

function kiemTraLoaiND(value, hienThiErr) {
    if (value == "GV" || value == "HV") {
        document.getElementById(hienThiErr).style.display = "none";
        return true;
    }
    else {
        document.getElementById(hienThiErr).innerText = `Vui lòng chọn loại người dùng`;
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
}

function kiemTraLoaiNN(value, hienThiErr) {
    if (value == "ITALIAN" || value == "FRENCH" || value == "JAPANESE" || value == "CHINESE" || value == "RUSSIAN" || value == "SWEDEN" || value == "SPANISH") {
        document.getElementById(hienThiErr).style.display = "none";
        return true;
    }
    else {
        document.getElementById(hienThiErr).innerText = `Vui lòng chọn loại ngôn ngữ`;
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
}

function kiemTraDoDai(value,hienThiErr, max) {
    var length = value.length;
    if (length > max) {
        document.getElementById(hienThiErr).innerText = `Độ dài không vượt quá ${max} kí tự`;
        document.getElementById(hienThiErr).style.display = "inline-block";
        return false;
    }
    else {
        document.getElementById(hienThiErr).style.display = "none";
        return true;
    }
}