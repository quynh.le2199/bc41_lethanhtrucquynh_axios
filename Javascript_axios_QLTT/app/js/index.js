const BASE_URL = "https://63c75704dcdc478e15d3435b.mockapi.io";

function fetchUserList() {
    batLoading();
    axios({
        url: `${BASE_URL}/user`,
        method: "GET",
    }).then(function(res) {
        tatLoading();
        renderUserList(res.data);
    }).catch(function(err) {
        tatLoading();
    });
}
fetchUserList();

function xoaUser(id) {
    batLoading();
    axios({
        url: `${BASE_URL}/user/${id}`,
        method: "DELETE",
    }).then(function(res) {
        tatLoading();
        fetchUserList();
    }).catch(function (err) {
        tatLoading();
    })
}

function themUser() {
    axios({
        url: `${BASE_URL}/user`,
        method: "GET",
    }).then(function(res) {
        var user = layThongTinTuForm();
        var isValidTaiKhoan = true;
        isValidTaiKhoan = kiemTraTrong(user.taiKhoan, "tbTaiKhoan") && kiemTraTrung(user.taiKhoan, res.data, "tbTaiKhoan");
        // isValidTaiKhoan = kiemTraTrong(user.taiKhoan, "tbTaiKhoan");
        var isValidHoTen = true;
        isValidHoTen =  kiemTraTrong(user.hoTen, "tbHoTen") && kiemTraChu(user.hoTen,"tbHoTen");
        var isValidMatKhau = true;
        isValidMatKhau = kiemTraTrong(user.matKhau, "tbMatKhau") && kiemTraMatKhau(user.matKhau, "tbMatKhau");
        var isValidEmail = true;
        isValidEmail = kiemTraTrong(user.email, "tbEmail") && kiemTraEmail(user.email, "tbEmail");
        var isValidHinhAnh = true;
        isValidHinhAnh = kiemTraTrong(user.hinhAnh, "tbHinhAnh");
        var isValidLoaiND = true;
        isValidLoaiND =kiemTraLoaiND(user.loaiND, "tbLoaiNguoiDung");
        var isValidNgonNgu = true;
        isValidNgonNgu =kiemTraLoaiNN(user.ngonNgu, "tbLoaiNgonNgu");
        var isValidMoTa = true;
        isValidMoTa = kiemTraTrong(user.moTa, "tbMoTa") && kiemTraDoDai(user.moTa, "tbMoTa", 60);

        var isValid = isValidTaiKhoan & isValidHoTen & isValidMatKhau & isValidEmail & isValidHinhAnh & isValidLoaiND & isValidNgonNgu & isValidMoTa;
        if (isValid) {
            batLoading();
            axios({
                url: `${BASE_URL}/user/`,
                method: "POST",
                data: user,
            }).then(function(res){
                tatLoading();
                fetchUserList();
            }).catch(function(err) {
                tatLoading();
            })
        }
    }).catch(function(err) {
        tatLoading();
    });
    
    
}

function suaUser(id) {
    batLoading();
    axios({
        url: `${BASE_URL}/user/${id}`,
        method: "GET",
    }).then(function(res) {
        tatLoading();
        renderCapNhatButton(res.data);
        showThongTinLenForm(res.data);
    }).catch(function (err) {
        tatLoading();
        console.log(err);
    })
    
}

function capNhatUser(id) {
    batLoading();
    var data = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/user/${id}`,
        method: "PUT",
        data: data,
    }).then(function(res) {
        tatLoading();
        fetchUserList();
    }).catch(function(err) {
        tatLoading();
    })
}