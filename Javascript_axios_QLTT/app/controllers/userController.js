function batLoading() {
    document.getElementById("loading").style.display = "flex";
}

function tatLoading() {
    document.getElementById("loading").style.display = "none";
}

function renderUserList(userArr) {
    var contentHTML = "";
    userArr.forEach(function(user) {
        var contentTr = `<tr>
                            <td>${user.id}</td>
                            <td>${user.taiKhoan}</td>
                            <td>${user.matKhau}</td>
                            <td>${user.hoTen}</td>
                            <td>${user.email}</td>
                            <td>${user.ngonNgu}</td>
                            <td>${user.loaiND}</td>
                            <td>
                                <button onclick="suaUser(${user.id})" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Sửa</button>
                                <button onclick="xoaUser(${user.id})" class="btn btn-danger">Xóa</button>
                            </td>
                         </tr>`;
        contentHTML += contentTr;
    });
    document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
}

function layThongTinTuForm() {
    var taiKhoan = document.getElementById("TaiKhoan").value;
    var hoTen = document.getElementById("HoTen").value;
    var matKhau = document.getElementById("MatKhau").value;
    var email = document.getElementById("Email").value;
    var loaiND = document.getElementById("loaiNguoiDung").value;
    var ngonNgu = document.getElementById("loaiNgonNgu").value;
    var moTa = document.getElementById("MoTa").value;
    var hinhAnh = document.getElementById("HinhAnh").value;

    return {
        taiKhoan: taiKhoan,
        hoTen: hoTen, 
        matKhau: matKhau,
        email: email,
        loaiND: loaiND,
        ngonNgu: ngonNgu,
        moTa: moTa,
        hinhAnh: hinhAnh,
    };
}

function showThongTinLenForm(user) {
    document.getElementById("TaiKhoan").value = user.taiKhoan;
    document.getElementById("HoTen").value = user.hoTen;
    document.getElementById("MatKhau").value = user.matKhau;
    document.getElementById("Email").value = user.email;
    document.getElementById("loaiNguoiDung").value = user.loaiND;
    document.getElementById("loaiNgonNgu").value = user.ngonNgu;
    document.getElementById("MoTa").value = user.moTa;
    document.getElementById("HinhAnh").value = user.hinhAnh;
}

function renderCapNhatButton(user) {
    var contentHTML = `<button onclick="capNhatUser(${user.id})" type="button" class="btn btn-success">Cập
        nhật</button>`;
    document.getElementById("capNhat").innerHTML = contentHTML;
}